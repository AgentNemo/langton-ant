package gui

import (
	"github.com/disintegration/imaging"
	"github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/AgentNemo/langton-ant/internal"
	"image"
	"image/color"
	"time"
)

type GameUI struct {
	Game       *internal.Game
	width      int
	height     int
	AntImage   image.Image
	Delay      time.Duration
	Running    bool
	TileMargin int
}

func NewGame(width int, height int, game *internal.Game, img image.Image) *GameUI {
	ebiten.SetWindowSize(width, height)
	ebiten.SetWindowTitle("Langton Ant")
	return &GameUI{width: width, height: height, Game: game, AntImage: img, TileMargin: 2}
}

func (g *GameUI) init() {
	time.Sleep(time.Second * 2)
	g.Running = true
}

func (g *GameUI) Update() error {
	if !g.Running {
		return nil
	}
	time.Sleep(g.Delay)
	g.Game.Round++
	g.Game.Next()
	return nil
}

func (g *GameUI) Draw(screen *ebiten.Image) {
	screen.Fill(color.Black)
	tileWidth := g.width / (g.Game.World.Width)
	tileHeight := g.height / g.Game.World.Height
	antImageResize := imaging.Resize(g.AntImage, tileWidth-g.TileMargin, tileHeight-g.TileMargin, imaging.Lanczos)
	tileImageFalse := ebiten.NewImage(tileWidth-g.TileMargin, tileHeight-g.TileMargin)
	tileImageFalse.Fill(color.White)
	tileImageTrue := ebiten.NewImage(tileWidth-g.TileMargin, tileHeight-g.TileMargin)
	tileImageTrue.Fill(color.Gray16{Y: 0x55ff})
	for i := 0; i < g.Game.World.Height; i++ {
		for ii := 0; ii < g.Game.World.Width; ii++ {
			op := &ebiten.DrawImageOptions{}
			x := ii * tileWidth
			y := i * tileHeight
			op.GeoM.Translate(float64(x), float64(y))
			if g.Game.World.GetSquare(ii, i) {
				screen.DrawImage(tileImageTrue, op)
			} else {
				screen.DrawImage(tileImageFalse, op)
			}
		}
	}

	for _, ant := range g.Game.Ants {
		antImageTrans := antImageResize
		switch ant.Direction {
		case internal.RIGHT:
			antImageTrans = imaging.Rotate270(antImageTrans)
		case internal.DOWN:
			antImageTrans = imaging.Rotate180(antImageTrans)
		case internal.LEFT:
			antImageTrans = imaging.Rotate90(antImageTrans)
		}
		op := &ebiten.DrawImageOptions{}
		x := ant.X * tileWidth
		y := ant.Y * tileHeight
		op.GeoM.Translate(float64(x), float64(y))
		screen.DrawImage(ebiten.NewImageFromImage(antImageTrans), op)
	}
}

func (g *GameUI) Layout(outsideWidth, outsideHeight int) (int, int) {
	return g.width, g.height
}

func (g *GameUI) Create(delay time.Duration) error {
	g.Delay = delay
	go g.init()
	return ebiten.RunGame(g)
}
