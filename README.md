# Langton Ant

Simple implementation of the game https://en.wikipedia.org/wiki/Langton%27s_ant

# Run

````shell script
go run start.go
````

Check out the cli arguments
````shell script
go.exe run start.go --help
````

## Tests

Run

````shell script
go test -race -coverprofile=coverage.out -covermode=atomic ./... && go tool cover -html=coverage.out -o corverage.html
````