module gitlab.com/AgentNemo/langton-ant

go 1.16

require (
	github.com/disintegration/imaging v1.6.2
	github.com/hajimehoshi/ebiten/v2 v2.2.2
	github.com/stretchr/testify v1.7.0
)
