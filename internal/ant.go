package internal

// Ant Direction
const (
	UP = iota
	RIGHT
	DOWN
	LEFT
)

// Ant representation
type Ant struct {
	X         int
	Y         int
	Direction int
	Rule      AntRule
}

// NewAnt creates a new ant with the given start position
func NewAnt(startX int, startY int) *Ant {
	return &Ant{X: startX, Y: startY, Direction: LEFT, Rule: AntRuleDefault()}
}

// SetRule sets custom rules for the ant to behaviour
func (a *Ant) SetRule(rule AntRule) {
	a.Rule = rule
}

// Move moves the ant int he given World
func (a *Ant) Move(world *World) {
	a.Rule(a, world)
}

// turnRight turns the ant direction right
func (a *Ant) turnRight() {
	a.Direction = (a.Direction + 1) % 4
}

// turnLeft turns the ant direction left
func (a *Ant) turnLeft() {
	a.Direction--
	if a.Direction < 0 {
		a.Direction = LEFT
	}
}

// moveForward moves the ant one square forward depending on the direction
func (a *Ant) moveForward(width int, height int) {
	switch a.Direction {
	case LEFT:
		a.X--
		if a.X < 0 {
			a.X = width - 1
		}
	case RIGHT:
		a.X++
		if a.X >= width {
			a.X = 0
		}
	case UP:
		a.Y--
		if a.Y < 0 {
			a.Y = height - 1
		}
	case DOWN:
		a.Y++
		if a.Y >= height {
			a.Y = 0
		}
	}
}

// IsOnSquare returns if the ant is on the given square
func (a Ant) IsOnSquare(x int, y int) bool {
	return a.X == x && a.Y == y
}
