package internal

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewAnt(t *testing.T) {
	ant := NewAnt(2, 3)
	assert.Equal(t, ant.X, 2)
	assert.Equal(t, ant.Y, 3)
}

func TestAnt_IsOnSquare(t *testing.T) {
	ant := NewAnt(2, 3)
	assert.True(t, ant.IsOnSquare(2, 3))
	assert.False(t, ant.IsOnSquare(3, 3))
}

func TestAnt_Move(t *testing.T) {
	ant := NewAnt(2, 3)
	ant.moveForward(5, 5)
	assert.True(t, ant.IsOnSquare(1, 3))

	ant = NewAnt(0, 3)
	ant.moveForward(5, 5)
	assert.True(t, ant.IsOnSquare(4, 3))
}
