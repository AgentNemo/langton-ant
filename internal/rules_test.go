package internal

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAntRuleDefault(t *testing.T) {
	world := NewWorld(10, 10)
	ant := NewAnt(5, 5)
	ant.Move(world)
	assert.Equal(t, ant.Direction, UP)
	assert.True(t, ant.IsOnSquare(5, 4))
	assert.True(t, world.GetSquare(5, 5))

	ant.Move(world)
	assert.Equal(t, ant.Direction, RIGHT)
	assert.True(t, ant.IsOnSquare(6, 4))
	assert.True(t, world.GetSquare(5, 4))

	ant.Move(world)
	assert.Equal(t, ant.Direction, DOWN)
	assert.True(t, ant.IsOnSquare(6, 5))
	assert.True(t, world.GetSquare(6, 4))

	ant.Move(world)
	assert.Equal(t, ant.Direction, LEFT)
	assert.True(t, ant.IsOnSquare(5, 5))
	assert.True(t, world.GetSquare(6, 5))


	ant.Move(world)
	assert.Equal(t, ant.Direction, DOWN)
	assert.True(t, ant.IsOnSquare(5, 6))
	assert.False(t, world.GetSquare(5, 5))

	ant.Move(world)
	assert.Equal(t, ant.Direction, LEFT)
	assert.True(t, ant.IsOnSquare(4, 6))
	assert.True(t, world.GetSquare(5, 6))

	ant.Move(world)
	assert.Equal(t, ant.Direction, UP)
	assert.True(t, ant.IsOnSquare(4, 5))
	assert.True(t, world.GetSquare(4, 6))

	ant.Move(world)
	assert.Equal(t, ant.Direction, RIGHT)
	assert.True(t, ant.IsOnSquare(5, 5))
	assert.True(t, world.GetSquare(4, 5))

	ant.Move(world)
	assert.Equal(t, ant.Direction, DOWN)
	assert.True(t, ant.IsOnSquare(5, 6))
	assert.True(t, world.GetSquare(5, 5))
}
