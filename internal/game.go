package internal

type Game struct {
	Round int
	World *World
	Ants  []*Ant
}

func NewGame(world *World, ants ...*Ant) *Game {
	return &Game{World: world, Ants: ants, Round: 0}
}

func (g *Game) Next() {
	for _, ant := range g.Ants {
		ant.Move(g.World)
	}
}
