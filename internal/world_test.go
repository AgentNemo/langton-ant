package internal

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewWorld(t *testing.T) {
	world := NewWorld(40, 50)
	assert.Equal(t, world.Width, 40)
	assert.Equal(t, world.Height, 50)
}

func TestWorld_Toggle(t *testing.T) {
	world := NewWorld(40, 50)
	world.Toggle(14, 17)
	assert.True(t, world.GetSquare(14, 17))
}
