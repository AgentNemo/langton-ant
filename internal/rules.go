package internal

type AntRule func(ant *Ant, world *World)

// AntRuleDefault Default ant rules described https://en.wikipedia.org/wiki/Langton%27s_ant
func AntRuleDefault() AntRule {
	return func(ant *Ant, world *World) {
		currentState := world.GetSquare(ant.X, ant.Y)
		if !currentState {
			ant.turnRight()
		} else {
			ant.turnLeft()
		}
		world.Toggle(ant.X, ant.Y)
		ant.moveForward(world.Width, world.Height)
	}
}
