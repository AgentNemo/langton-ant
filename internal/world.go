package internal

import "fmt"

// World world definition
type World struct {
	Width   int
	Height  int
	Squares [][]bool
}

// NewWorld creates a new world with the given artificial boundaries
func NewWorld(width int, height int) *World {
	squares := make([][]bool, height)
	for i := range squares {
		squares[i] = make([]bool, width)
	}
	return &World{Width: width, Height: height, Squares: squares}
}

// Toggle flips a square
func (w *World) Toggle(x int, y int) {
	w.Squares[y][x] = !w.Squares[y][x]
}

// GetSquare returns the state of a square
func (w World) GetSquare(x int, y int) bool {
	return w.Squares[y][x]
}

// Print prints the world to the console. The given ants are also drawn
func (w *World) Print(ants ...*Ant) {
	for i := 0; i < w.Height; i++ {
		for ii := 0; ii < w.Width; ii++ {
			draw := "F"
			if w.GetSquare(ii, i) {
				draw = "T"
			}
			for _, ant := range ants {
				if ant.IsOnSquare(ii, i) {
					draw = "A"
					break
				}
			}
			fmt.Print(fmt.Sprintf(" %s ", draw))
		}
		fmt.Println()
	}
	fmt.Println()
}
