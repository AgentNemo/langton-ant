package main

import (
	"flag"
	"gitlab.com/AgentNemo/langton-ant/gui"
	"gitlab.com/AgentNemo/langton-ant/internal"
	"image"
	_ "image/jpeg"
	"log"
	"os"
	"time"
)

var (
	ScreenWidthDefault  = 500
	ScreenHeightDefault = 500
	GameWidthDefault    = 10
	GameHeightDefault   = 10
)

func main() {
	screenWidth := flag.Int("swidth", ScreenWidthDefault, "screen width in pixel")
	screenHeight := flag.Int("sheight", ScreenWidthDefault, "screen height in pixel")

	gameWidth := flag.Int("gwidth", GameWidthDefault, "game width")
	gameHeight := flag.Int("gheight", GameHeightDefault, "game height")

	delay := flag.Int("delay", 1000, "delay between rounds in ms")

	flag.Parse()

	world := internal.NewWorld(*gameWidth, *gameHeight)
	ant := internal.NewAnt((world.Width/2)-1, (world.Height/2)-1)
	game := internal.NewGame(world, ant)

	file, err := os.Open("assets/ant.jpg")
	defer file.Close()
	if err != nil {
		log.Fatal(err)
	}
	img, _, err := image.Decode(file)
	if err != nil {
		log.Fatal(err)
	}
	gameUI := gui.NewGame(*screenWidth, *screenHeight, game, img)
	err = gameUI.Create(time.Millisecond * time.Duration(*delay))
	if err != nil {
		log.Fatal(err)
	}
}
